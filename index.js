var canvas = document.getElementById("canvas");
var ctx = canvas.getContext('2d');

canvas.width = 1080;
canvas.height = 720;
var radius = 5;
var drag = false;
var textmode = false;
var start_x ; 
var start_y ;
var url = "";
ctx.lineWidth = radius*2;
ctx.fillStyle = "white";
ctx.fillRect(0, 0, canvas.width, canvas.height);

var array = new Array();
var steps = -1;

//////////////////////////////////////////////////////////////

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left ,
        y: evt.clientY - rect.top 
    };
}

//////////////////////////////////////////////////////////////

function init(){
    canvas.width = 1080;
    canvas.height = 720; 
    var drag = false;
    ctx.lineWidth = radius*2;
    ctx.fillStyle = "white";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    steps = -1;
    array = [];
    push();
    url = canvas.toDataURL();
}

//////////////////////////////////////////////////////////////

var circlebutton = document.getElementsByClassName('circle');
var tributton = document.getElementsByClassName('triangle');
var squarebutton = document.getElementsByClassName('square');

for (var i = 0; i < circlebutton.length; i++) {
    circlebutton[i].addEventListener('click', draw_circle);
}
for (var i = 0; i < tributton.length; i++) {
    tributton[i].addEventListener('click', draw_triangle);
}
for (var i = 0; i < squarebutton.length; i++) {
    squarebutton[i].addEventListener('click', draw_square);
}

function draw_circle(evt){
    let pos = getMousePos(canvas, evt);
    var _circle = evt.target;
    var circle_active = document.getElementsByClassName('c_down')[0];
    var triangle_active = document.getElementsByClassName('t_down')[0];
    var square_active = document.getElementsByClassName('s_down')[0];
    var erase_active = document.getElementsByClassName('down')[0];
    if(!circle_active){
        _circle.className += ' c_down';
    }
    if(circle_active){
        circle_active.className = 'circle';
    }
    if(triangle_active){
        triangle_active.className = 'triangle';
    }
    if(square_active){
        square_active.className = 'square';
    }
    if(erase_active){
        erase_active.className = 'eraser';
        setSwatch({target: document.getElementsByClassName('swatch')[0]});
    }
}

function draw_triangle(evt){
    let pos = getMousePos(canvas, evt);
    var _triangle = evt.target;
    var circle_active = document.getElementsByClassName('c_down')[0];
    var triangle_active = document.getElementsByClassName('t_down')[0];
    var square_active = document.getElementsByClassName('s_down')[0];
    var erase_active = document.getElementsByClassName('down')[0];
    if(!triangle_active){
        _triangle.className += ' t_down';
    }
    if(circle_active){
        circle_active.className = 'circle';
    }
    if(triangle_active){
        triangle_active.className = 'triangle';
    }
    if(square_active){
        square_active.className = 'square';
    }
    if(erase_active){
        erase_active.className = 'eraser';
        setSwatch({target: document.getElementsByClassName('swatch')[0]});
    }
}

function draw_square(evt){
    let pos = getMousePos(canvas, evt);
    var _square = evt.target;
    var circle_active = document.getElementsByClassName('c_down')[0];
    var triangle_active = document.getElementsByClassName('t_down')[0];
    var square_active = document.getElementsByClassName('s_down')[0];
    var erase_active = document.getElementsByClassName('down')[0];
    if(!square_active){
        _square.className += ' s_down';
    }
    if(circle_active){
        circle_active.className = 'circle';
    }
    if(triangle_active){
        triangle_active.className = 'triangle';
    }
    if(square_active){
        square_active.className = 'square';
    }
    if(erase_active){
        erase_active.className = 'eraser';
        setSwatch({target: document.getElementsByClassName('swatch')[0]});
    }
}

//////////////////////////////////////////////////////////////

var setrad = function(newrad){
    if(newrad < minrad){
        newrad = minrad;
    }
    else if(newrad > maxrad){
        newrad = maxrad;
    }
    radius = newrad;
    ctx.lineWidth = radius*2;
    radspan.innerHTML = radius;
}

var minrad = 1;
var maxrad = 25;
var defaultrad = 5;
var interval = 1;
var radspan = document.getElementById('radval');
var decrad = document.getElementById('decrad');
var incrad = document.getElementById('incrad');

decrad.addEventListener('click', function(){
    setrad(radius - interval);
});

incrad.addEventListener('click', function(){
    setrad(radius + interval);
})

//////////////////////////////////////////////////////////////

var colors = ['black', 'grey', 'red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet'];
var swatches = document.getElementsByClassName('swatch');

for(var i=0; i<colors.length; i++){
    var swatch = document.createElement('div');
    swatch.className = 'swatch';
    swatch.style.backgroundColor = colors[i];
    swatch.addEventListener('click', setSwatch);
    document.getElementById('colors').appendChild(swatch);
}

function setcolor(color){
    ctx.fillStyle = color;
    ctx.strokeStyle = color;
    var active = document.getElementsByClassName('active')[0];
    var erase_active = document.getElementsByClassName('down')[0];
    if(active){
        active.className = 'swatch';
    }
    if(erase_active){
        erase_active.className = 'eraser';
    }
}

function setSwatch(evt){
    var swatch = evt.target;
    setcolor(swatch.style.backgroundColor);
    if(swatch.style.backgroundColor == 'black'){
        canvas.style.cursor = "url('img/black.png'), auto";
    }
    if(swatch.style.backgroundColor == 'grey'){
        canvas.style.cursor = "url('img/grey.png'), auto";
    }
    if(swatch.style.backgroundColor == 'red'){
        canvas.style.cursor = "url('img/red.png'), auto";
    }
    if(swatch.style.backgroundColor == 'orange'){
        canvas.style.cursor = "url('img/orange.png'), auto";
    }
    if(swatch.style.backgroundColor == 'yellow'){
        canvas.style.cursor = "url('img/yellow.png'), auto";
    }
    if(swatch.style.backgroundColor == 'green'){
        canvas.style.cursor = "url('img/green.png'), auto";
    }
    if(swatch.style.backgroundColor == 'blue'){
        canvas.style.cursor = "url('img/blue.png'), auto";
    }
    if(swatch.style.backgroundColor == 'indigo'){
        canvas.style.cursor = "url('img/indigo.png'), auto";
    }
    if(swatch.style.backgroundColor == 'violet'){
        canvas.style.cursor = "url('img/violet.png'), auto";
    }
    swatch.className += ' active';
}

setSwatch({target: document.getElementsByClassName('swatch')[0]});

//////////////////////////////////////////////////////////////

var erasebutton = document.getElementsByClassName('eraser');

for (var i = 0; i < erasebutton.length; i++) {
    erasebutton[i].addEventListener('click', erase);
}

function erase(evt){
    var _erase = evt.target;
    ctx.fillStyle = "white";
    ctx.strokeStyle = "white";
    var active = document.getElementsByClassName('active')[0];
    var circle_active = document.getElementsByClassName('circle')[0];
    var triangle_active = document.getElementsByClassName('triangle')[0];
    var square_active = document.getElementsByClassName('square')[0];
    var erase_active = document.getElementsByClassName('down')[0];
    if(active){
        active.className = 'swatch';
    }
    if(circle_active){
        circle_active.className = 'circle';
    }
    if(triangle_active){
        triangle_active.className = 'triangle';
    }
    if(square_active){
        square_active.className = 'square';
    }
    if(erase_active){
        erase_active.className = 'eraser';
    }
    canvas.style.cursor = "url('img/eraser_cursor.png'), auto";
    _erase.className += ' down';
}

//////////////////////////////////////////////////////////////

var undobutton = document.getElementById('undo');
var redobutton = document.getElementById('redo');

undobutton.addEventListener('click', undo);
redobutton.addEventListener('click', redo);

function push(){
    steps++;
    if (steps < array.length){ 
        array.length = steps; 
    }
    array.push(canvas.toDataURL());
}

function undo(){
    if (steps > 0) {
        steps--;
        var canvasPic = new Image();
        canvasPic.src = array[steps];
        canvasPic.onload = function(){ 
            ctx.drawImage(canvasPic, 0, 0, canvas.width, canvas.height);
        }
    }
}

function redo(){
    if (steps < array.length) {
        steps++;
        var canvasPic = new Image();
        canvasPic.src = array[steps];
        canvasPic.onload = function(){ 
            ctx.drawImage(canvasPic, 0, 0, canvas.width, canvas.height); 
        }
    }
}

//////////////////////////////////////////////////////////////

var imageLoader = document.getElementById('file-input');
imageLoader.addEventListener('change', handleImage, false);

function handleImage(e){
    var reader = new FileReader();
    reader.onload = function(event) {
        var img = new Image();
        img.onload = function() {
            ctx.drawImage(img, 0, 0);
            ctx.lineWidth = radius*2;
            push();
            url = canvas.toDataURL();
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]);
}

//////////////////////////////////////////////////////////////

function saveimage(link, filename){
    link.href = canvas.toDataURL();
    link.download = filename;
}

document.getElementById('save').addEventListener('click', function(){
    saveimage(this, Math.random().toString(8).substr(2, 9));
}, false);

//////////////////////////////////////////////////////////////

var clearbutton = document.getElementById('clear');

clearbutton.addEventListener('click', clearcanvas);

function clearcanvas(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    init();
    var active = document.getElementsByClassName('active')[0];
    var erase_active = document.getElementsByClassName('down')[0];
    if(active){
        active.className = 'swatch';
    }
    if(erase_active){
        erase_active.className = 'eraser';
    }
    setSwatch({target: document.getElementsByClassName('swatch')[0]});
    canvas.style.cursor = "url('img/black.png'), auto";
}

////////////////////////////////////////////////////////////////

var inputbutton = document.getElementById('textinput');
var textsize = document.getElementById('textsize');
var fonttype = document.getElementById('font');

inputbutton.addEventListener('click', mode);

function mode(evt){
    textmode = !textmode;
}

function input(x, y){
    var input = document.createElement('input');

    input.type = 'text';
    input.style.position = 'fixed';
    input.style.left = (x+370) + 'px';
    input.style.top = (y+115) + 'px';
    input.style.border = '2px solid #000000';
    input.onkeydown = enter;

    document.body.appendChild(input);

    input.focus();

    isInput = true;
}

function enter(evt){
    var key = evt.keyCode;
    if(key === 13){
        text(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
        document.body.removeChild(this);
        isInput = false;
        textmode = false;
    }
}

function text(txt, x, y){
    ctx.textBaseLine = 'top';
    ctx.textAlign = 'left';
    if(fonttype.value == ""){
        ctx.font = textsize.value + "px Nadeem";
    }
    else{
        ctx.font = textsize.value + "px " + fonttype.value;
    }

    ctx.fillText(txt, x-408, y-101);
}

////////////////////////////////////////////////////////////////

var putPoint = function(evt){
    let pos = getMousePos(canvas, evt);
    var circle_active = document.getElementsByClassName('c_down')[0];
    var triangle_active = document.getElementsByClassName('t_down')[0];
    var square_active = document.getElementsByClassName('s_down')[0];
    if(drag && !circle_active && !triangle_active && !square_active){
        ctx.lineTo(pos.x, pos.y);
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(pos.x, pos.y, radius, 0, Math.PI*2);
        ctx.fill();
        ctx.beginPath();
        ctx.moveTo(pos.x, pos.y);
    }
    if(drag && circle_active && !triangle_active && !square_active){
        ctx.clearRect(0,0,canvas.width,canvas.height);
        animation();
        ctx.beginPath();
        ctx.arc(start_x, start_y, Math.sqrt(Math.pow(pos.x-start_x, 2) + Math.pow(pos.y-start_y, 2)), 0, Math.PI*2);
        ctx.stroke();
    }
    if(drag && !circle_active && !triangle_active && square_active){
        ctx.clearRect(0,0,canvas.width,canvas.height);
        animation();
        ctx.strokeRect(start_x, start_y, pos.x-start_x, pos.y-start_y);
    }
    if(drag && !circle_active && triangle_active && !square_active){
        ctx.clearRect(0,0,canvas.width,canvas.height);
        animation();
        ctx.beginPath();
        ctx.moveTo(pos.x, pos.y);
        ctx.lineTo(start_x,start_y);
        ctx.lineTo(pos.x+(pos.x-start_x),start_y);
        ctx.closePath();
        ctx.stroke();
    }
}

function animation(){
    var image = new Image();
    image.src = url;
    ctx.drawImage(image, 0, 0);
}

var engage = function(evt){
    let pos = getMousePos(canvas, evt);
    if(textmode){
        textmode = false;
        input(pos.x, pos.y);
    }
    else{
        start_x = pos.x;
        start_y = pos.y;
        drag = true;
        ctx.beginPath();
        ctx.moveTo(start_x,start_y);
    }
}

var disengage = function(){
    if(drag){
        push();
    }
    url = canvas.toDataURL();
    drag = false;
    ctx.beginPath();
}

push();
url = canvas.toDataURL();
canvas.addEventListener('mousedown', engage, false);
canvas.addEventListener('mouseup', disengage, false);
canvas.addEventListener('mouseleave', disengage, false);
canvas.addEventListener('mousemove', putPoint, false);

//////////////////////////////////////////////////////////////