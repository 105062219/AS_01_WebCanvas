# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

    Functions in canvas

1.  筆刷與橡皮擦
    利用mouseup mousedown mouseleave mousemove去listen滑鼠的行為，決定是否要engage與disengage，再決定drag的布林值。
    putPoint是負責筆刷路徑的function，會幫忙處理畫過去之後上顏色。
2.  顏色選擇
    使用setSwatch與setcolor這兩個function，可交替使用不同顏色。
3.  筆刷大小
    setrad負責調整筆刷的粗度，使用radius控制
4.  文字輸入
    使用textmode決定點下去的當下是否為文字輸入模式，再利用input enter text三個function，完成這個功能。先在Font Type輸入想要的文字樣式，點下Input Text這個按鍵後，可在canvas的任意位置點下，輸入想要顯示的文字後按Enter，即可出現文字。
5.  游標
    canvas.style.cursor可以修改游標的樣式。
6.  Reset
    clearcanvas可以清理canvas上的塗鴉，再呼叫init重製整個canvas。
7.  Shapes
    putPoint裡另外設了3個if statement，判斷circle triangle square active的狀態決定要進入哪一個。
8.  Undo Redo
    使用push undo redo三個function實現這個功能。在一開始load和畫完圖的時候將canvas的畫面push進array，如果要undo或redo就從array裡面取出來。
9.  Load
    使用handleimage這個function實現了這個功能。使用filereader將圖片從使用者端取出來，再將它drawImage到canvas上。
10. Download
    在save function裡使用toDataURL將canvas存成圖片。